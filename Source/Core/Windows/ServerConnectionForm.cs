﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeImp.DoomBuilder.Windows
{
	public partial class ServerConnectionForm : Form
	{
		#region ================== Constructor

		public ServerConnectionForm()
		{
			InitializeComponent();

			address.Text = General.Settings.ServerAddress;
			name.Text = General.Settings.ClientName;
		}

		#endregion

		#region ================== Events

		private void apply_Click(object sender, EventArgs e)
		{
			General.Settings.ServerAddress = address.Text;
			General.Settings.ClientName = name.Text;

			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		#endregion
	}
}
