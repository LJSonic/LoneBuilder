﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CodeImp.DoomBuilder.Controls
{
	public partial class ChatBox : UserControl
	{
		#region ================== Variables

		private Queue<int> recentMessageLengths = new Queue<int>();

		#endregion

		#region ================== Constructor / Disposer

		public ChatBox()
		{
			InitializeComponent();
		}

		#endregion

		#region ================== Events

		private void inputBox_ContentsResized(object sender, ContentsResizedEventArgs e)
		{
			int heightDelta = e.NewRectangle.Height - inputBox.DisplayRectangle.Height;
			if (heightDelta == 0)
				return;

			inputBox.Height += heightDelta;
			UpdateHeight();
		}

		private void inputBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && !e.Shift)
			{
				new NetworkAction("builder_say").Add(inputBox.Text).Send();
				Close();
				e.Handled = true;
			}
			else
			{
				OnKeyDown(sender, e);
			}
		}

		private void messageBox_ContentsResized(object sender, ContentsResizedEventArgs e)
		{
			int heightDelta = Math.Min(e.NewRectangle.Height, 400) - messageBox.ClientRectangle.Height;

			if (messageBox.ScrollBars != RichTextBoxScrollBars.Vertical && e.NewRectangle.Height > 400)
				messageBox.ScrollBars = RichTextBoxScrollBars.Vertical;

			if (heightDelta == 0)
				return;

			messageBox.Height += heightDelta;
			UpdateHeight();
		}

		private void recentMessageBox_ContentsResized(object sender, ContentsResizedEventArgs e)
		{
			int heightDelta = Math.Min(e.NewRectangle.Height, 400) - recentMessageBox.ClientRectangle.Height;
			if (heightDelta == 0)
				return;

			recentMessageBox.Height += heightDelta;
			UpdateHeight();
		}

		private void recentMessageTimer_Tick(object sender, EventArgs e)
		{
			if (recentMessageBox.SelectionLength != 0)
				return;

			int length = recentMessageLengths.Dequeue();
			bool last = recentMessageLengths.Count == 0;

			recentMessageBox.Select(0, last ? length : length + 1);
			recentMessageBox.ReadOnly = false;
			recentMessageBox.SelectedText = "";
			recentMessageBox.ReadOnly = true;

			if (last)
			{
				Hide();
				recentMessageBox.Hide();
				UpdateHeight();

				recentMessageTimer.Stop();
			}
			else
			{
				StartRecentMessageTimer(length);
			}
		}

		private void OnKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				Close();
				e.Handled = true;
			}
		}

		#endregion

		#region ================== Methods

		private void UpdateHeight()
		{
			int newHeight = 0;
			if (inputBox.Visible)
				newHeight += inputBox.Height;
			if (messageBox.Visible)
				newHeight += messageBox.Height;
			if (recentMessageBox.Visible)
				newHeight += recentMessageBox.Height;

			// Keep the bottom of the chat box at the same position
			Top += Height;
			Top -= newHeight;

			Height = newHeight;
		}

		private void StartRecentMessageTimer(int textLength)
		{
			recentMessageTimer.Interval = 5000 + textLength * 100;
			recentMessageTimer.Start();
		}

		public void Open()
		{
			Show();
			inputBox.Show();
			messageBox.Show();
			recentMessageBox.Hide();

			UpdateHeight();

			recentMessageBox.Clear();
			recentMessageLengths.Clear();
			recentMessageTimer.Stop();

			inputBox.Focus();
		}

		public void AddMessage(string text)
		{
			int selectionStart = messageBox.SelectionStart;
			int selectionLength = messageBox.SelectionLength;

			if (messageBox.Text.Length != 0)
				messageBox.AppendText(Environment.NewLine);
			messageBox.AppendText(text);

			if (selectionLength != 0)
			{
				messageBox.SelectionStart = selectionStart;
				messageBox.SelectionLength = selectionLength;
			}

			// If the chat box wasn't manually open, show temporary messages
			if (!inputBox.Visible)
			{
				Show();
				recentMessageBox.Show();

				if (recentMessageBox.Text.Length != 0)
					recentMessageBox.AppendText(Environment.NewLine);

				int oldTotalLength = recentMessageBox.TextLength;
				recentMessageBox.AppendText(text);
				recentMessageLengths.Enqueue(recentMessageBox.TextLength - oldTotalLength);

				if (recentMessageLengths.Count == 1)
					StartRecentMessageTimer(text.Length);

				UpdateHeight();
			}
		}

		public void Close()
		{
			Hide();
			inputBox.Hide();
			messageBox.Hide();
			recentMessageBox.Hide();

			inputBox.Clear();

			recentMessageBox.Clear();
			recentMessageLengths.Clear();
			recentMessageTimer.Stop();

			General.Interface.FocusDisplay();
		}

		#endregion
	}
}
