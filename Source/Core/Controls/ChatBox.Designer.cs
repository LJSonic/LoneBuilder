﻿namespace CodeImp.DoomBuilder.Controls
{
	partial class ChatBox
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.inputBox = new System.Windows.Forms.RichTextBox();
			this.messageBox = new System.Windows.Forms.RichTextBox();
			this.recentMessageBox = new System.Windows.Forms.RichTextBox();
			this.recentMessageTimer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// inputBox
			// 
			this.inputBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
			this.inputBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.inputBox.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.inputBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.inputBox.ForeColor = System.Drawing.Color.White;
			this.inputBox.Location = new System.Drawing.Point(0, 0);
			this.inputBox.Margin = new System.Windows.Forms.Padding(0);
			this.inputBox.MaxLength = 1000;
			this.inputBox.Name = "inputBox";
			this.inputBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.inputBox.Size = new System.Drawing.Size(500, 16);
			this.inputBox.TabIndex = 0;
			this.inputBox.Text = "";
			this.inputBox.Visible = false;
			this.inputBox.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.inputBox_ContentsResized);
			this.inputBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.inputBox_KeyDown);
			// 
			// messageBox
			// 
			this.messageBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
			this.messageBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.messageBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.messageBox.ForeColor = System.Drawing.Color.White;
			this.messageBox.HideSelection = false;
			this.messageBox.Location = new System.Drawing.Point(0, 0);
			this.messageBox.Margin = new System.Windows.Forms.Padding(0);
			this.messageBox.Name = "messageBox";
			this.messageBox.ReadOnly = true;
			this.messageBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.messageBox.Size = new System.Drawing.Size(500, 0);
			this.messageBox.TabIndex = 0;
			this.messageBox.Text = "";
			this.messageBox.Visible = false;
			this.messageBox.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.messageBox_ContentsResized);
			this.messageBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
			// 
			// recentMessageBox
			// 
			this.recentMessageBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
			this.recentMessageBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.recentMessageBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.recentMessageBox.ForeColor = System.Drawing.Color.White;
			this.recentMessageBox.HideSelection = false;
			this.recentMessageBox.Location = new System.Drawing.Point(0, 0);
			this.recentMessageBox.Margin = new System.Windows.Forms.Padding(0);
			this.recentMessageBox.Name = "recentMessageBox";
			this.recentMessageBox.ReadOnly = true;
			this.recentMessageBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.recentMessageBox.Size = new System.Drawing.Size(500, 0);
			this.recentMessageBox.TabIndex = 1;
			this.recentMessageBox.Text = "";
			this.recentMessageBox.Visible = false;
			this.recentMessageBox.ContentsResized += new System.Windows.Forms.ContentsResizedEventHandler(this.recentMessageBox_ContentsResized);
			this.recentMessageBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
			// 
			// recentMessageTimer
			// 
			this.recentMessageTimer.Tick += new System.EventHandler(this.recentMessageTimer_Tick);
			// 
			// ChatBox
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
			this.Controls.Add(this.recentMessageBox);
			this.Controls.Add(this.messageBox);
			this.Controls.Add(this.inputBox);
			this.Margin = new System.Windows.Forms.Padding(0);
			this.Name = "ChatBox";
			this.Size = new System.Drawing.Size(500, 16);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox inputBox;
		private System.Windows.Forms.RichTextBox messageBox;
		private System.Windows.Forms.RichTextBox recentMessageBox;
		private System.Windows.Forms.Timer recentMessageTimer;
	}
}
