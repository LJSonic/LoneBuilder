
#region ================== Copyright (c) 2007 Pascal vd Heiden

/*
 * Copyright (c) 2007 Pascal vd Heiden, www.codeimp.com
 * This program is released under GNU General Public License
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

#endregion

#region ================== Namespaces

using System;
using System.Reflection;

#endregion

namespace CodeImp.DoomBuilder
{
	/// <summary>
	/// This binds a method to a network action.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class NetworkActionAttribute : Attribute
	{
		#region ================== Properties

		/// <summary>
		/// Set this to the name of the plugin library when this action is defined by another plugin. The library name is the filename without extension.
		/// </summary>
		public string Library { get; set; }

		internal string Name { get; }

		public bool RequiresMap { get; set; } = true;

		/// <summary>
		/// Redraw the interface after calling this action
		/// </summary>
		public bool Redraw { get; set; } = true;

		/// <summary>
		/// Backup all marks before calling this action, and restore them after the call
		/// </summary>
		public bool RestoreMarks { get; set; } = true;

		#endregion

		#region ================== Constructor / Disposer

		/// <summary>
		/// This binds a method to a network action.
		/// </summary>
		/// <param name="name">The action name.</param>
		public NetworkActionAttribute(string name)
		{
			// Initialize
			this.Name = name;
			this.Library = "";
		}

		#endregion

		#region ================== Methods

		// This makes the proper name
		public string GetFullName(Assembly asm)
		{
			string asmname;

			if(Library.Length > 0)
				asmname = Library.ToLowerInvariant();
			else
				asmname = asm.GetName().Name.ToLowerInvariant();

			return asmname + "_" + Name;
		}

		#endregion
	}
}
