﻿
#region ================== Copyright (c) 2007 Pascal vd Heiden

/*
 * Copyright (c) 2007 Pascal vd Heiden, www.codeimp.com
 * This program is released under GNU General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#endregion

#region ================== Namespaces

using CodeImp.DoomBuilder.Editing;
using CodeImp.DoomBuilder.Geometry;
using CodeImp.DoomBuilder.Map;
using CodeImp.DoomBuilder.VisualModes;

#endregion

namespace CodeImp.DoomBuilder
{
	public class Mapper
	{
		#region ================== Variables

		private bool visualMode;

		#endregion

		#region ================== Properties

		public int Id { get; set; }
		public bool IsMe { get { return (this == General.Network.Me); } }
		public string Name { get; set; } = "LJ Sonic";
		public Vector3D Position { get; set; }
		public Vector2D ViewSize { get; set; } = new Vector2D(1000f, 1000f);
		public float Angle { get; set; }
		public Thing Thing { get; set; }

		public bool VisualMode {
			get { return visualMode; }

			set
			{
				// If the mapper is leaving visual mode and we had created
				// a dummy thing to represent the mapper, delete the thing
				if (!value && Thing != null && !Thing.IsDisposed)
				{
					General.Map.Map.BeginAddRemove();
					Thing.Dispose();
					General.Map.Map.EndAddRemove(); //mxd
					General.Map.ThingsFilter.Update();
				}

				visualMode = value;
			}
		}

		#endregion

		#region ================== Constructor

		public Mapper(int id)
		{
			Id = id;
		}

		#endregion

		#region ================== Methods

		public void UpdateThing()
		{
			VisualMode visualMode = General.Editing.Mode as VisualMode;

			if (Thing != null && !Thing.IsDisposed)
			{
				if (Thing.Position != Position)
				{
					// Move horizontally
					visualMode.BlockMap.RemoveThing(Thing);
					Thing.Move(Position);
					visualMode.BlockMap.AddThing(Thing);

					// Determine sector
					if (Thing.Sector == null || Thing.Sector.IsDisposed || !Thing.Sector.Intersect(Thing.Position))
						Thing.DetermineSector(visualMode.BlockMap);

					// Move vertically
					int floorHeight = Thing.Sector?.FloorHeight ?? 0;
					Thing.Move(Position.x, Position.y, Position.z - floorHeight);
				}

				Thing.Rotate(Angle - Angle2D.PI);
			}
			else
			{
				Thing = General.Map.Map.CreateThing();
				if (Thing == null)
					return;

				Thing.FullType = 3329;
				Thing.Rotate(Angle - Angle2D.PI);

				// Move horizontally
				Thing.Move(Position);
				visualMode.BlockMap.AddThing(Thing);
				Thing.DetermineSector(visualMode.BlockMap);

				// Move vertically
				int floorHeight = Thing.Sector?.FloorHeight ?? 0;
				Thing.Move(Position.x, Position.y, Position.z - floorHeight);

				Thing.UpdateConfiguration();

				// Update things filter so that it includes this thing
				General.Map.ThingsFilter.Update();
			}
		}

		#endregion
	}
}
