﻿
#region ================== Copyright (c) 2007 Pascal vd Heiden

/*
 * Copyright (c) 2007 Pascal vd Heiden, www.codeimp.com
 * This program is released under GNU General Public License
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 */

#endregion

#region ================== Namespaces

using CodeImp.DoomBuilder.Geometry;
using System;
using System.Collections.Generic;

#endregion

namespace CodeImp.DoomBuilder
{
	public sealed class NetworkAction
	{
		#region ================== Properties

		public List<byte> SentBytes { get; set; }
		public byte[] ReceivedBytes { get; set; }
		public int Position { get; set; }
		public string Name { get; }
		public Mapper Actor { get; set; }
		public bool Udp { get; }

		#endregion

		#region ================== Constructors

		public NetworkAction(string name, bool udp = false)
		{
			SentBytes = new List<byte>();

			Udp = udp;
			Name = name;
			Actor = General.Network.Me;

			if (udp)
				Add(Actor?.Id ?? -1);
			Add(name);
			Add(Actor?.Id ?? -1);
		}

		public NetworkAction(byte[] bytes)
		{
			ReceivedBytes = bytes;
			Name = GetString();
		}

		#endregion

		public sbyte GetInt8()
		{
			sbyte n = (sbyte)ReceivedBytes[Position];
			Position += sizeof(sbyte);
			return n;
		}

		public byte GetUint8()
		{
			byte n = ReceivedBytes[Position];
			Position += sizeof(byte);
			return n;
		}

		public short GetInt16()
		{
			byte[] bytes = new byte[sizeof(short)];
			Array.Copy(ReceivedBytes, Position, bytes, 0, sizeof(short));
			Position += sizeof(short);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			return BitConverter.ToInt16(bytes, 0);
		}

		public int GetInt32()
		{
			byte[] bytes = new byte[sizeof(int)];
			Array.Copy(ReceivedBytes, Position, bytes, 0, sizeof(int));
			Position += sizeof(int);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			return BitConverter.ToInt32(bytes, 0);
		}

		public float GetFloat()
		{
			byte[] bytes = new byte[sizeof(float)];
			Array.Copy(ReceivedBytes, Position, bytes, 0, sizeof(float));
			Position += sizeof(float);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			return BitConverter.ToSingle(bytes, 0);
		}

		public bool GetBool()
		{
			byte[] bytes = new byte[sizeof(bool)];
			Array.Copy(ReceivedBytes, Position, bytes, 0, sizeof(bool));
			Position += sizeof(bool);
			return BitConverter.ToBoolean(bytes, 0);
		}

		public char GetChar()
		{
			byte[] bytes = new byte[sizeof(char)];
			Array.Copy(ReceivedBytes, Position, bytes, 0, sizeof(char));
			Position += sizeof(char);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			return BitConverter.ToChar(bytes, 0);
		}
		
		public string GetString()
		{
			int length = GetInt32();
			string s = "";
			for (int i = 0; i < length; i++)
				s += GetChar();
			return s;
		}

		public Vector2D GetVector2D()
		{
			float x = GetFloat();
			float y = GetFloat();
			return new Vector2D(x, y);
		}

		public Vector3D GetVector3D()
		{
			float x = GetFloat();
			float y = GetFloat();
			float z = GetFloat();
			return new Vector3D(x, y, z);
		}

		public void GetDrawingOptions()
		{
			General.Settings.DefaultFloorHeight   = GetInt32();
			General.Settings.DefaultCeilingHeight = GetInt32();
			General.Settings.DefaultBrightness    = GetInt32();

			General.Map.Options.DefaultTopTexture     = GetString();
			General.Map.Options.DefaultWallTexture    = GetString();
			General.Map.Options.DefaultBottomTexture  = GetString();
			General.Map.Options.DefaultFloorTexture   = GetString();
			General.Map.Options.DefaultCeilingTexture = GetString();
			General.Map.Options.CustomFloorHeight     = GetInt32();
			General.Map.Options.CustomCeilingHeight   = GetInt32();
			General.Map.Options.CustomBrightness      = GetInt32();

			General.Map.Options.OverrideFloorTexture   = GetBool();
			General.Map.Options.OverrideCeilingTexture = GetBool();
			General.Map.Options.OverrideTopTexture     = GetBool();
			General.Map.Options.OverrideMiddleTexture  = GetBool();
			General.Map.Options.OverrideBottomTexture  = GetBool();
			General.Map.Options.OverrideFloorHeight    = GetBool();
			General.Map.Options.OverrideCeilingHeight  = GetBool();
			General.Map.Options.OverrideBrightness     = GetBool();
		}

		public void Add(sbyte n)
		{
			SentBytes.Add((byte)n);
		}

		public void Add(byte n)
		{
			SentBytes.Add(n);
		}

		public void Add(short n)
		{
			byte[] bytes = BitConverter.GetBytes(n);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			SentBytes.AddRange(bytes);
		}

		public NetworkAction Add(int n)
		{
			byte[] bytes = BitConverter.GetBytes(n);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			SentBytes.AddRange(bytes);
			return this;
		}

		public void Add(float n)
		{
			byte[] bytes = BitConverter.GetBytes(n);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			SentBytes.AddRange(bytes);
		}

		public void Add(bool n)
		{
			byte[] bytes = BitConverter.GetBytes(n);
			SentBytes.AddRange(bytes);
		}

		public void Add(char c)
		{
			byte[] bytes = BitConverter.GetBytes(c);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			SentBytes.AddRange(bytes);
		}

		public NetworkAction Add(string s)
		{
			Add(s.Length);
			for (int i = 0; i < s.Length; i++)
				Add(s[i]);
			return this;
		}

		public void Add(Vector2D v)
		{
			Add((float)v.x);
			Add((float)v.y);
		}

		public void Add(Vector3D v)
		{
			Add((float)v.x);
			Add((float)v.y);
			Add((float)v.z);
		}

		public void AddDrawingOptions()
		{
			Add(General.Settings.DefaultFloorHeight);
			Add(General.Settings.DefaultCeilingHeight);
			Add(General.Settings.DefaultBrightness);

			Add(General.Map.Options.DefaultTopTexture);
			Add(General.Map.Options.DefaultWallTexture);
			Add(General.Map.Options.DefaultBottomTexture);
			Add(General.Map.Options.DefaultFloorTexture);
			Add(General.Map.Options.DefaultCeilingTexture);
			Add(General.Map.Options.CustomFloorHeight);
			Add(General.Map.Options.CustomCeilingHeight);
			Add(General.Map.Options.CustomBrightness);

			Add(General.Map.Options.OverrideFloorTexture);
			Add(General.Map.Options.OverrideCeilingTexture);
			Add(General.Map.Options.OverrideTopTexture);
			Add(General.Map.Options.OverrideMiddleTexture);
			Add(General.Map.Options.OverrideBottomTexture);
			Add(General.Map.Options.OverrideFloorHeight);
			Add(General.Map.Options.OverrideCeilingHeight);
			Add(General.Map.Options.OverrideBrightness);
		}

		public void Send()
		{
			if (General.Network.Online)
			{
				if (Udp)
				{
					General.Network.SendUdpPacket(SentBytes.ToArray());
				}
				else
				{
					General.Network.Send(SentBytes.Count);
					General.Network.Send(SentBytes.ToArray());
				}
			}
			else
			{
				var action = new NetworkAction(SentBytes.ToArray());
				General.Network.AddActionToQueue(action);
			}
		}
	}
}
