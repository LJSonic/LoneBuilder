﻿
#region ================== Copyright (c) 2007 Pascal vd Heiden

/*
 * Copyright (c) 2007 Pascal vd Heiden, www.codeimp.com
 * This program is released under GNU General Public License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#endregion

#region ================== Namespaces

using CodeImp.DoomBuilder;
using CodeImp.DoomBuilder.Actions;
using CodeImp.DoomBuilder.Geometry;
using CodeImp.DoomBuilder.Map;
using CodeImp.DoomBuilder.VisualModes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;

#endregion

namespace CodeImp.DoomBuilder
{
	public sealed class NetworkManager : IDisposable
	{
		#region ================== Delegates

		internal delegate void NetworkActionDelegate(NetworkAction action);

		#endregion

		#region ================== Variables

		private static Dictionary<string, NetworkActionDelegate> actions = new Dictionary<string, NetworkActionDelegate>();
		private static Dictionary<string, NetworkActionAttribute> actionAttributes = new Dictionary<string, NetworkActionAttribute>();

		private TcpClient tcpClient;
		private NetworkStream tcpStream;
		private UdpClient udpClient;
		private Thread thread;
		private Queue<NetworkAction> pendingActions = new Queue<NetworkAction>();
		private Queue<NetworkAction> pendingMapActions = new Queue<NetworkAction>();

		private NetworkAction keepAlivePacket;
		private float lastKeepAlivePacket;
		private object keepAlivePacketLock = new object();

		private int prevDefaultFloorHeight;
		private int prevDefaultCeilingHeight;
		private int prevDefaultBrightness;

		private string prevDefaultTopTexture;
		private string prevDefaultWallTexture;
		private string prevDefaultBottomTexture;
		private string prevDefaultFloorTexture;
		private string prevDefaultCeilingTexture;
		private int    prevCustomFloorHeight;
		private int    prevCustomCeilingHeight;
		private int    prevCustomBrightness;

		private bool prevOverrideFloorTexture;
		private bool prevOverrideCeilingTexture;
		private bool prevOverrideTopTexture;
		private bool prevOverrideMiddleTexture;
		private bool prevOverrideBottomTexture;
		private bool prevOverrideFloorHeight;
		private bool prevOverrideCeilingHeight;
		private bool prevOverrideBrightness;

		#endregion

		#region ================== Properties

		public bool IsDisposed { get; private set; }
		public bool Online { get; set; }
		public List<Mapper> Mappers { get; } = new List<Mapper>();
		public Mapper Me { get; set; }
		public bool ActionRunning { get; set; }
		public string SessionDirName { get; set; }
		public bool Connected { get; set; }
		public bool Connecting { get; set; }
		public FileStream Map { get; set; }
		public int MapSize { get; set; }
		public float LastPositionUpdate { get; set; }
		public bool ActionIncoming { get { return pendingActions.Count != 0 || pendingMapActions.Count != 0; } }

		#endregion

		#region ================== Constructor / Disposer

		public NetworkManager()
		{
			General.Actions.BindMethods(this);

			// We have no destructor
			GC.SuppressFinalize(this);
		}

		public void Dispose()
		{
			if (IsDisposed)
				return;

			if (Online)
				UninitialiseOnlineSession();

			General.Actions.UnbindMethods(this);

			IsDisposed = true;
		}

		#endregion

		#region ================== Methods

		public void InitialiseOfflineSession()
		{
			Online = false;

			Connected = true;

			var mapper = new Mapper(0);
			mapper.Name = "You";
			Mappers.Clear();
			Mappers.Add(mapper);
			General.Network.Me = mapper;
		}

		public void InitialiseOnlineSession()
		{
			Online = true;

			SessionDirName = "Sessions/" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss fff");
			Directory.CreateDirectory(SessionDirName);

			pendingActions.Clear();
			pendingMapActions.Clear();

			General.Network.Me = null;
			Mappers.Clear();

			tcpClient = new TcpClient(General.Settings.ServerAddress, 5029);
			tcpStream = tcpClient.GetStream();

			udpClient = new UdpClient(General.Settings.ServerAddress, 5029);

			Connected = true;

			new NetworkAction("builder_connect").Add(General.Settings.ClientName).Send();

			lastKeepAlivePacket = Clock.CurrentTime;

			thread = new Thread(HandleNetwork);
			thread.Start();
		}

		public void UninitialiseOnlineSession()
		{
			Connected = false;
			if (thread != null)
				thread.Join();

			if (tcpClient != null)
				tcpClient.Close();
			tcpClient = null;
			tcpStream = null;

			if (udpClient != null)
				udpClient.Close();
			udpClient = null;

			pendingActions.Clear();
			pendingMapActions.Clear();

			General.Network.Me = null;
			Mappers.Clear();

			Online = false;
		}

		public Mapper GetMapper(int id)
		{
			foreach (Mapper mapper in Mappers)
			{
				if (mapper.Id == id)
					return mapper;
			}
			return null;
		}

		public void SendUdpPacket(byte[] bytes)
		{
			lock (udpClient)
				udpClient.Send(bytes, bytes.Length);
		}

		private byte[] ReceiveBytes(int n)
		{
			var bytes = new byte[n];
			int numReceivedBytes = 0;

			while (numReceivedBytes != n)
				numReceivedBytes += tcpStream.Read(bytes, numReceivedBytes, n - numReceivedBytes);

			return bytes;
		}

		private int ReceiveInt32()
		{
			var bytes = ReceiveBytes(sizeof(int));
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			return BitConverter.ToInt32(bytes, 0);
		}

		public void Send(byte[] bytes)
		{
			lock (tcpClient)
			{
				if (!Connected)
					return;

				try
				{
					tcpStream.Write(bytes, 0, bytes.Length);
				}
				catch (IOException)
				{
					Connected = false;
				}
			}
		}

		public void Send(int n)
		{
			byte[] bytes = BitConverter.GetBytes(n);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);
			Send(bytes);
		}

		private void HandleNetwork()
		{
			while (true)
			{
				lock (tcpClient)
				{
					if (!Connected)
						return;

					while (tcpStream.DataAvailable)
					{
						int length;
						byte[] bytes;
						try
						{
							length = ReceiveInt32();
							bytes = ReceiveBytes(length);
						}
						catch (SocketException)
						{
							Connected = false;
							return;
						}

						var action = new NetworkAction(bytes);
						AddActionToQueue(action);
					}
				}

				lock (udpClient)
				{
					while (udpClient.Available > 0)
					{
						var endPoint = udpClient.Client.RemoteEndPoint as IPEndPoint;
						var action = new NetworkAction(udpClient.Receive(ref endPoint));
						AddActionToQueue(action);
					}
				}

				// Send a keepalive packet from time to time
				if (Clock.CurrentTime - lastKeepAlivePacket >= 5000.0f)
				{
					lock (keepAlivePacketLock)
					{
						if (keepAlivePacket != null)
						{
							keepAlivePacket.Send();
							lastKeepAlivePacket = Clock.CurrentTime;
						}
					}
				}

				Thread.Sleep(50);
			}
		}

		public void HandlePendingActions()
		{
			while (true)
			{
				NetworkAction action = null;

				lock (pendingActions)
				{
					if (pendingActions.Count != 0)
						action = pendingActions.Dequeue();
				}

				if (action == null && General.Map != null && General.Editing.Mode != null)
				{
					lock (pendingMapActions)
					{
						if (pendingMapActions.Count != 0)
							action = pendingMapActions.Dequeue();
					}
				}

				if (action == null)
					return;

				ExecuteAction(action);
			}
		}

		public void AddActionToQueue(NetworkAction action)
		{
			// Ignore invalid actions
			if (!actionAttributes.ContainsKey(action.Name))
				return;

			if (actionAttributes[action.Name].RequiresMap)
			{
				lock (pendingMapActions)
					pendingMapActions.Enqueue(action);
			}
			else
			{
				lock (pendingActions)
					pendingActions.Enqueue(action);
			}
		}

		public void ExecuteAction(NetworkAction action)
		{
			Editing.EditMode prevMode = General.Editing.Mode;

			List<Vertex>  markedVertices = null;
			List<Linedef> markedLines    = null;
			List<Sidedef> markedSides    = null;
			List<Sector>  markedSectors  = null;
			List<Thing>   markedThings   = null;

			action.Actor = GetMapper(action.GetInt32());

			if (actionAttributes[action.Name].RestoreMarks && General.Map != null)
			{
				markedVertices = General.Map.Map.GetMarkedVertices(true);
				markedLines    = General.Map.Map.GetMarkedLinedefs(true);
				markedSides    = General.Map.Map.GetMarkedSidedefs(true);
				markedSectors  = General.Map.Map.GetMarkedSectors (true);
				markedThings   = General.Map.Map.GetMarkedThings  (true);
				General.Map.Map.ClearAllMarks(false);

				General.Map.Map.ClearAllNetworkMarks();
				General.Map.Map.GeometryChanged = false;
				General.Map.Map.PopulationChanged = false;
			}

			if (prevMode != null)
				prevMode.OnNetworkActionBegin(action);

			ActionRunning = true;
			actions[action.Name].Invoke(action);
			ActionRunning = false;

			if (prevMode != null && General.Editing.Mode == prevMode)
				prevMode.OnNetworkActionEnd(action);

			if (actionAttributes[action.Name].RestoreMarks && General.Map != null && markedVertices != null)
			{
				General.Map.Map.ClearAllMarks(false);
				foreach (var v in markedVertices)
					if (!v.IsDisposed)
						v.Marked = true;
				foreach (var l in markedLines)
					if (!l.IsDisposed)
						l.Marked = true;
				foreach (var s in markedSides)
					if (!s.IsDisposed)
						s.Marked = true;
				foreach (var s in markedSectors)
					if (!s.IsDisposed)
						s.Marked = true;
				foreach (var t in markedThings)
					if (!t.IsDisposed)
						t.Marked = true;
			}

			if (actionAttributes[action.Name].Redraw)
				General.Interface.RedrawDisplay();

			if (General.Map != null)
				General.Map.Map.RemovedElements.Clear();
		}

		public void SendPositionUpdate()
		{
			if (!(General.Editing.Mode is VisualMode))
			{
				var action = new NetworkAction("builder_cam2d", true);

				action.Add(new Vector2D(General.Map.Renderer2D.OffsetX, General.Map.Renderer2D.OffsetY));
				action.Add(new Vector2D(General.Map.CRenderer2D.Viewport.Width, Math.Abs(General.Map.CRenderer2D.Viewport.Height)));

				action.Send();
			}
			else
			{
				var action = new NetworkAction("builder_cam3d", true);

				action.Add(General.Map.VisualCamera.Position);
				action.Add(General.Map.VisualCamera.AngleXY);

				action.Send();
			}

			LastPositionUpdate = Clock.CurrentTime;
		}

		public void StoreDrawingSettings()
		{
			prevDefaultFloorHeight   = General.Settings.DefaultFloorHeight;
			prevDefaultCeilingHeight = General.Settings.DefaultCeilingHeight;
			prevDefaultBrightness    = General.Settings.DefaultBrightness;

			prevDefaultTopTexture     = General.Map.Options.DefaultTopTexture;
			prevDefaultWallTexture    = General.Map.Options.DefaultWallTexture;
			prevDefaultBottomTexture  = General.Map.Options.DefaultBottomTexture;
			prevDefaultFloorTexture   = General.Map.Options.DefaultFloorTexture;
			prevDefaultCeilingTexture = General.Map.Options.DefaultCeilingTexture;
			prevCustomFloorHeight     = General.Map.Options.CustomFloorHeight;
			prevCustomCeilingHeight   = General.Map.Options.CustomCeilingHeight;
			prevCustomBrightness      = General.Map.Options.CustomBrightness;

			prevOverrideFloorTexture   = General.Map.Options.OverrideFloorTexture;
			prevOverrideCeilingTexture = General.Map.Options.OverrideCeilingTexture;
			prevOverrideTopTexture     = General.Map.Options.OverrideTopTexture;
			prevOverrideMiddleTexture  = General.Map.Options.OverrideMiddleTexture;
			prevOverrideBottomTexture  = General.Map.Options.OverrideBottomTexture;
			prevOverrideFloorHeight    = General.Map.Options.OverrideFloorHeight;
			prevOverrideCeilingHeight  = General.Map.Options.OverrideCeilingHeight;
			prevOverrideBrightness     = General.Map.Options.OverrideBrightness;
		}

		public void RestoreDrawingSettings()
		{
			General.Settings.DefaultFloorHeight   = prevDefaultFloorHeight;
			General.Settings.DefaultCeilingHeight = prevDefaultCeilingHeight;
			General.Settings.DefaultBrightness    = prevDefaultBrightness;

			General.Map.Options.DefaultTopTexture     = prevDefaultTopTexture;
			General.Map.Options.DefaultWallTexture    = prevDefaultWallTexture;
			General.Map.Options.DefaultBottomTexture  = prevDefaultBottomTexture;
			General.Map.Options.DefaultFloorTexture   = prevDefaultFloorTexture;
			General.Map.Options.DefaultCeilingTexture = prevDefaultCeilingTexture;
			General.Map.Options.CustomFloorHeight     = prevCustomFloorHeight;
			General.Map.Options.CustomCeilingHeight   = prevCustomCeilingHeight;
			General.Map.Options.CustomBrightness      = prevCustomBrightness;

			General.Map.Options.OverrideFloorTexture   = prevOverrideFloorTexture;
			General.Map.Options.OverrideCeilingTexture = prevOverrideCeilingTexture;
			General.Map.Options.OverrideTopTexture     = prevOverrideTopTexture;
			General.Map.Options.OverrideMiddleTexture  = prevOverrideMiddleTexture;
			General.Map.Options.OverrideBottomTexture  = prevOverrideBottomTexture;
			General.Map.Options.OverrideFloorHeight    = prevOverrideFloorHeight;
			General.Map.Options.OverrideCeilingHeight  = prevOverrideCeilingHeight;
			General.Map.Options.OverrideBrightness     = prevOverrideBrightness;
		}

		[BeginAction("openchatbox")]
		public void OpenChatBox()
		{
			General.Interface.OpenChatBox();
		}

		// This binds all methods marked with the NetworkAction attribute
		public static void BindMethods(Type type)
		{
			General.WriteLogLine("Binding network action methods for " + type.Name + " object...");

			// Go for all methods on class
			MethodInfo[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
			foreach (MethodInfo method in methods)
			{
				// Check if the method has this attribute
				NetworkActionAttribute[] attrs = (NetworkActionAttribute[])method.GetCustomAttributes(typeof(NetworkActionAttribute), true);

				// Go for all attributes
				foreach (NetworkActionAttribute action in attrs)
				{
					// Create a delegate for this method
					NetworkActionDelegate del = (NetworkActionDelegate)Delegate.CreateDelegate(typeof(NetworkActionDelegate), method);

					// Make proper name
					string name = action.GetFullName(type.Assembly);

					// Bind method to action
					General.WriteLogLine("Adding network action " + name);
					if (!actions.ContainsKey(name))
					{
						actions.Add(name, del);
						actionAttributes.Add(name, action);
					}
					else
						throw new ArgumentException("Could not bind " + method.ReflectedType.Name + "." + method.Name + " to network action \"" + name + "\", that network action already exists!");
				}
			}
		}

		[NetworkAction("mapperlist", RequiresMap = false)]
		public static void NetworkAction_MapperList(NetworkAction action)
		{
			int numMappers = action.GetInt32();
			for (int i = 0; i < numMappers; i++)
			{
				int id = action.GetInt32();
				var mapper = new Mapper(id);
				mapper.Name = action.GetString();
				General.Network.Mappers.Add(mapper);
			}
		}

		[NetworkAction("addmapper", RequiresMap = false)]
		public static void NetworkAction_Connect(NetworkAction action)
		{
			int id = action.GetInt32();
			var joiner = new Mapper(id);
			joiner.Name = action.GetString();
			General.Network.Mappers.Add(joiner);

			if (General.Network.Me == null)
			{
				General.Network.Me = joiner;
				lock (General.Network.keepAlivePacketLock)
					General.Network.keepAlivePacket = new NetworkAction("", true);
			}

			General.Interface.AddChatMessage($"* {joiner.Name} joined the server.");
		}

		[NetworkAction("removemapper", RequiresMap = false)]
		public static void NetworkAction_Disconnect(NetworkAction action)
		{
			int id = action.GetInt32();
			Mapper mapper = General.Network.GetMapper(id);

			General.Interface.AddChatMessage($"* {mapper.Name} left the server.");

			General.Network.Mappers.Remove(mapper);
		}

		[NetworkAction("cam2d", Redraw = false, RestoreMarks = false)]
		public static void NetworkAction_ClassicCameraPosition(NetworkAction action)
		{
			if (action.Actor == null)
				return;

			action.Actor.VisualMode = false;

			action.Actor.Position = action.GetVector2D();
			action.Actor.ViewSize = action.GetVector2D();
		}

		[NetworkAction("cam3d", Redraw = false, RestoreMarks = false)]
		public static void NetworkAction_VisualCameraPosition(NetworkAction action)
		{
			if (action.Actor == null)
				return;

			action.Actor.VisualMode = true;

			action.Actor.Position = action.GetVector3D();
			action.Actor.Angle = action.GetFloat();

			if (!action.Actor.IsMe && General.Editing.Mode is VisualMode)
				action.Actor.UpdateThing();
		}

		[NetworkAction("say", Redraw = false, RestoreMarks = false)]
		public static void NetworkAction_Say(NetworkAction action)
		{
			if (action.Actor == null)
				return;

			var message = action.GetString();

			General.Interface.AddChatMessage($"<{action.Actor.Name}> {message}");
		}

		#endregion
	}
}
